﻿namespace BankingApp.Repository.Uow
{
    public interface IUnitOfWorkFactory
    {
        IUnitOfWork CreateNewUnitOfWork();
    }
}