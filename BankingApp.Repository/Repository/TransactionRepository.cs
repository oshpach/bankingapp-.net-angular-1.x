using System.Collections.Generic;
using System.Linq;
using BankingApp.Entities;
using BankingApp.Repository.DataContext;

namespace BankingApp.Repository.Repository
{
    public class TransactionRepository : RepositoryBase, ITransactionRepository
    {
        public TransactionRepository(DbDataContext dataContext)
            : base(dataContext)
        {
        }

        public List<Transaction> GetUserTransactions(int userId)
        {
            return _dataContext.Transaction.Where(x => x.UserId == userId).OrderByDescending(x => x.CreateDate).ToList();
        }
    }

    public interface ITransactionRepository
    {
        List<Transaction> GetUserTransactions(int userId);
    }
}