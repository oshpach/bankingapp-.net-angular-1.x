﻿using System.Collections.Generic;
using BankingApp.Entities;

namespace BankingApp.Repository.Repository
{
    public interface IUserRepository
    {
        User GetUser(int userId);
        User GetUser(string userName, string password);
        List<User> GetUsers(int userIdToExclude);
        bool UserExists(string userName);
        void CreateUser(User user);
    }
}