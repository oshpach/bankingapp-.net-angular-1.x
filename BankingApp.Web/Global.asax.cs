﻿using System;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Routing;
using BankingApp.Common.Interfaces;
using BankingApp.Common.InversionOfControl;
using BankingApp.Web.IoC;

namespace BankingApp.Web
{
    public class WebApiApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            Bootstrapper.Initialise(GlobalConfiguration.Configuration);

            AreaRegistration.RegisterAllAreas();
            GlobalConfiguration.Configure(WebApiConfig.Register);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
        }

        protected void Application_PostAuthenticateRequest(Object sender, EventArgs e)
        {
            var userAccessController = Ioc.Get<IUserAccessController>();
            userAccessController.PostAuthRequest();
        }
    }
}
