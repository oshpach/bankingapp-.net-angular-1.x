using System.Net;
using System.Net.Http;
using System.Web.Http;
using BankingApp.Common.Definitions;
using BankingApp.Common.Interfaces;
using BankingApp.Core.Services;

namespace BankingApp.Web.Areas.v1
{
    [Authorize]
    public class UserController : ApiController
    {
        private readonly IUserDataService _userDataService;
        private readonly IUserAccessController _userAccessController;

        public UserController(IUserDataService userDataService, IUserAccessController userAccessController)
        {
            _userDataService = userDataService;
            _userAccessController = userAccessController;
        }

        [Route("~/api/v1/User"), HttpGet]
        public HttpResponseMessage GetUsers()
        {
            var result = _userDataService.GetUsers(_userAccessController.UserId);

            if (result.Status == RequestResultStatus.Success)
            {
                return Request.CreateResponse(HttpStatusCode.OK, result);
            }
            else
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, result);
            }
        }
    }
}