﻿using System.Net;
using System.Net.Http;
using System.Web.Http;
using BankingApp.Common.Definitions;
using BankingApp.Common.Interfaces;
using BankingApp.Core.Services;
using BankingApp.ViewModels;

namespace BankingApp.Web.Areas.v1
{
    [Authorize]
    public class TransferController : ApiController
    {
        private readonly IUserAccessController _userAccessController;
        private readonly IBankAcountDataService _bankAcountDataService;

        public TransferController(IUserAccessController userAccessController, IBankAcountDataService bankAcountDataService)
        {
            _userAccessController = userAccessController;
            _bankAcountDataService = bankAcountDataService;
        }

        [Route("~/api/v1/Transfer"), HttpPost]
        public HttpResponseMessage Transfer(TransferModel transferModel)
        {
            if (transferModel == null || transferModel.Amount <= 0 || transferModel.ToUserId <= 0)
                return Request.CreateResponse(HttpStatusCode.BadRequest, "Model is not valid");

            var result = _bankAcountDataService.Transfer(_userAccessController.UserId, transferModel);

            if (result.Status == RequestResultStatus.Success)
            {
                return Request.CreateResponse(HttpStatusCode.OK, result);
            }
            else
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, result);
            }
        }
    }
}