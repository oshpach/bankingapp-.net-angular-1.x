﻿using System;
using System.Web;
using System.Web.Security;
using BankingApp.Common.Interfaces;
using BankingApp.Common.InversionOfControl;
using BankingApp.Common.Models;
using BankingApp.Core.Services;
using Newtonsoft.Json;

namespace BankingApp.Web.Infrastructure
{
    public class UserAccessController : IUserAccessController
    {
        public bool IsAuthenticated
        {
            get { return HttpContext.Current.User.Identity.IsAuthenticated; }
        }
        public int UserId
        {
            get { return ((UserPrincipal)HttpContext.Current.User).UserId; }
        }

        public bool AuthenticateUser(int userId)
        {
            var authTicket = CreateAuthTicket(userId);

            if (authTicket != null)
            {
                var cookie = CreateAuthCookie(authTicket);

                HttpContext.Current.Response.Cookies.Add(cookie);

                //to have authenticated user context we imidiately set it
                PostAuthRequest();

                return true;
            }
            return false;
        }

        private FormsAuthenticationTicket CreateAuthTicket(int userId)
        {
            var service = Ioc.Get<IUserDataService>();
            var user = service.GetUser(userId);
            if (user != null)
            {
                var serializeModel = new UserPrincipalSerializeModel();
                serializeModel.UserId = user.Id;
                serializeModel.Name = user.Name;

                string userData = JsonConvert.SerializeObject(serializeModel);
                return new FormsAuthenticationTicket(
                    1,
                    user.Id.ToString(),
                    DateTime.Now,
                    DateTime.Now.AddDays(30),
                    false,
                    userData);
            }

            return null;
        }

        private HttpCookie CreateAuthCookie(FormsAuthenticationTicket authenticationTicket)
        {
            string encTicket = FormsAuthentication.Encrypt(authenticationTicket);
            HttpCookie faCookie = new HttpCookie(FormsAuthentication.FormsCookieName, encTicket);

            return faCookie;
        }

        public void SignOutUser()
        {
            if (HttpContext.Current.User.Identity.IsAuthenticated)
            {
                HttpCookie cookie = HttpContext.Current.Request.Cookies[FormsAuthentication.FormsCookieName];
                cookie.Expires = DateTime.Now.AddMinutes(-5);
                HttpContext.Current.Response.Cookies.Add(cookie);
            }
        }

        public void UpdateUser(UserPrincipalSerializeModel serializeModel)
        {
            var newUser = new UserPrincipal(serializeModel);
            HttpContext.Current.User = newUser;
        }

        public void PostAuthRequest()
        {
            HttpCookie authCookie = HttpContext.Current.Request.Cookies[FormsAuthentication.FormsCookieName];
            if (authCookie != null)
            {
                var authTicket = FormsAuthentication.Decrypt(authCookie.Value);
                var serializeModel = JsonConvert.DeserializeObject<UserPrincipalSerializeModel>(authTicket.UserData);

                UpdateUser(serializeModel);
            }
            else
            {
                HttpContext.Current.User = new UserPrincipal(0, "");
            }
        }
    }
}