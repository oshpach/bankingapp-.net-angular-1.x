﻿using System.Web.Http.Dependencies;
using Microsoft.Practices.Unity;
using Microsoft.Practices.Unity.Mvc;

namespace BankingApp.Web.IoC
{
    public class UnityResolver : UnityDependencyResolver, IDependencyResolver
    {
        private readonly IUnityContainer _container;

        public UnityResolver(IUnityContainer container)
            : base(container)
        {
            _container = container;
        }

        public IDependencyScope BeginScope()
        {
            var child = _container.CreateChildContainer();
            return new UnityResolver(child);
        }

        public void Dispose()
        {
            _container.Dispose();
        }
    }
}