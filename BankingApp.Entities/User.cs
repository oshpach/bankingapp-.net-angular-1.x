﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace BankingApp.Entities
{
    public class User
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Password { get; set; }

        public double AccountBalance { get; set; }

        [Timestamp]
        public byte[] RowVersion { get; set; }

        public virtual ICollection<Transaction> Transactions { get; set; }
    }
}
