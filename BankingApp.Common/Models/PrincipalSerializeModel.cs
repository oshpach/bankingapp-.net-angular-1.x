﻿using BankingApp.Common.Interfaces;

namespace BankingApp.Common.Models
{
    public class UserPrincipalSerializeModel : IUserPrincipal
    {
        public int UserId { get; set; }
        public string Name { get; set; }
    }
}