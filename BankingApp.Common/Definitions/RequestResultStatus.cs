﻿namespace BankingApp.Common.Definitions
{
    public enum RequestResultStatus
    {
        Error = 0,
        Success = 1,
        Duplicate = 2
    }
}