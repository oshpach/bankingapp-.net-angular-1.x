﻿using System.Configuration;
using System.Linq;

namespace BankingApp.Common.Configuration
{
    public class AppSettings
    {
        private const string ConnectionStringKey = "MsSqlConnectionString";

        private static AppSettings _instance;
        private string _connectionString;

        public static AppSettings GetInstance()
        {
            if (_instance == null)
            {
                _instance = new AppSettings();
            }

            return _instance;
        }

        private AppSettings()
        {
            if (ConfigurationManager.ConnectionStrings.Cast<ConnectionStringSettings>().Any(connStr => connStr.Name == ConnectionStringKey))
            {
                _connectionString = ConfigurationManager.ConnectionStrings[ConnectionStringKey].ConnectionString;
            }
        }

        public string ConnectionString
        {
            get { return _connectionString; }
        }
    }
}