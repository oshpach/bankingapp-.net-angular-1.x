﻿namespace BankingApp.Common.Interfaces
{
    public interface IUserPrincipal
    {
        int UserId { get; set; }
        string Name { get; set; }
    }
}