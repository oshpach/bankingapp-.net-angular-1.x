﻿using System.Collections.ObjectModel;
using BankingApp.Common.Definitions;
using BankingApp.Core.Services.Impl;
using BankingApp.Entities;
using BankingApp.Repository.Repository;
using BankingApp.Repository.Uow;
using BankingApp.ViewModels;
using Moq;
using NUnit.Framework;

namespace BankingApp.Core.Test.Services.Impl
{
    [TestFixture]
    public class BankAcountDataServiceTest
    {
        [Test]
        public void Withdraw_NotEnoughMoney_Test()
        {
            //Arrange
            var userRepo = new Mock<IUserRepository>();
            userRepo.Setup(x => x.GetUser(It.IsAny<int>())).Returns(new User() {AccountBalance = 99});

            var uow = new Mock<IUnitOfWork>();
            uow.Setup(x => x.UserRepository).Returns(userRepo.Object);

            var uowf = new Mock<IUnitOfWorkFactory>();
            uowf.Setup(x => x.CreateNewUnitOfWork()).Returns(uow.Object);

            var service = new BankAcountDataService(uowf.Object);

            //Act
            var result = service.Withdraw(1,100);

            //Assert
            Assert.AreEqual(result.Status, RequestResultStatus.Error);
            Assert.AreEqual(result.Message, "Not enough money");
        }

        [Test]
        public void Withdraw_Success_Test()
        {
            //Arrange
            var userRepo = new Mock<IUserRepository>();
            userRepo.Setup(x => x.GetUser(It.IsAny<int>())).Returns(new User() {AccountBalance = 100, Transactions = new Collection<Transaction>()});

            var uow = new Mock<IUnitOfWork>();
            uow.Setup(x => x.UserRepository).Returns(userRepo.Object);

            var uowf = new Mock<IUnitOfWorkFactory>();
            uowf.Setup(x => x.CreateNewUnitOfWork()).Returns(uow.Object);

            var service = new BankAcountDataService(uowf.Object);

            //Act
            var result = service.Withdraw(1,100);

            //Assert
            Assert.AreEqual(result.Status, RequestResultStatus.Success);
            Assert.AreEqual(result.Obj, 0);
        }

        [Test]
        public void Withdraw_UserIsNotFound_Test()
        {
            //Arrange
            var userRepo = new Mock<IUserRepository>();
            userRepo.Setup(x => x.GetUser(It.IsAny<int>())).Returns((User)null);

            var uow = new Mock<IUnitOfWork>();
            uow.Setup(x => x.UserRepository).Returns(userRepo.Object);

            var uowf = new Mock<IUnitOfWorkFactory>();
            uowf.Setup(x => x.CreateNewUnitOfWork()).Returns(uow.Object);

            var service = new BankAcountDataService(uowf.Object);

            //Act
            var result = service.Withdraw(1,100);

            //Assert
            Assert.AreEqual(result.Status, RequestResultStatus.Error);
            Assert.AreEqual(result.Obj, 0);
            Assert.AreEqual(result.Message, "User is not found");
        }

        [Test]
        public void Deposit_c_Test()
        {
            //Arrange
            var userRepo = new Mock<IUserRepository>();
            userRepo.Setup(x => x.GetUser(It.IsAny<int>())).Returns(new User() {AccountBalance = 100, Transactions = new Collection<Transaction>()});

            var uow = new Mock<IUnitOfWork>();
            uow.Setup(x => x.UserRepository).Returns(userRepo.Object);

            var uowf = new Mock<IUnitOfWorkFactory>();
            uowf.Setup(x => x.CreateNewUnitOfWork()).Returns(uow.Object);

            var service = new BankAcountDataService(uowf.Object);

            //Act
            var result = service.Deposite(1,100);

            //Assert
            Assert.AreEqual(result.Status, RequestResultStatus.Success);
            Assert.AreEqual(result.Obj, 200);
        }

        [Test]
        public void Deposit_UserNotFound_Test()
        {
            //Arrange
            var userRepo = new Mock<IUserRepository>();
            userRepo.Setup(x => x.GetUser(It.IsAny<int>())).Returns((User)null);

            var uow = new Mock<IUnitOfWork>();
            uow.Setup(x => x.UserRepository).Returns(userRepo.Object);

            var uowf = new Mock<IUnitOfWorkFactory>();
            uowf.Setup(x => x.CreateNewUnitOfWork()).Returns(uow.Object);

            var service = new BankAcountDataService(uowf.Object);

            //Act
            var result = service.Deposite(1,100);

            //Assert
            Assert.AreEqual(result.Status, RequestResultStatus.Error);
            Assert.AreEqual(result.Obj, 0);
            Assert.AreEqual(result.Message, "User is not found");
        }

        [Test]
        public void Transfer_FirstUserNotFound_Test()
        {
            //Arrange
            var userRepo = new Mock<IUserRepository>();
            userRepo.Setup(x => x.GetUser(1)).Returns((User)null);
            userRepo.Setup(x => x.GetUser(2)).Returns(new User() { AccountBalance = 100, Transactions = new Collection<Transaction>() });

            var uow = new Mock<IUnitOfWork>();
            uow.Setup(x => x.UserRepository).Returns(userRepo.Object);

            var uowf = new Mock<IUnitOfWorkFactory>();
            uowf.Setup(x => x.CreateNewUnitOfWork()).Returns(uow.Object);

            var service = new BankAcountDataService(uowf.Object);

            //Act
            var result = service.Transfer(1,new TransferModel(){Amount = 100, ToUserId = 2});

            //Assert
            Assert.AreEqual(result.Status, RequestResultStatus.Error);
            Assert.AreEqual(result.Obj, 0);
            Assert.AreEqual(result.Message, "User is not found");
        }

        [Test]
        public void Transfer_SecondUserNotFound_Test()
        {
            //Arrange
            var userRepo = new Mock<IUserRepository>();
            userRepo.Setup(x => x.GetUser(1)).Returns(new User() { AccountBalance = 100, Transactions = new Collection<Transaction>() });
            userRepo.Setup(x => x.GetUser(2)).Returns((User)null);

            var uow = new Mock<IUnitOfWork>();
            uow.Setup(x => x.UserRepository).Returns(userRepo.Object);

            var uowf = new Mock<IUnitOfWorkFactory>();
            uowf.Setup(x => x.CreateNewUnitOfWork()).Returns(uow.Object);

            var service = new BankAcountDataService(uowf.Object);

            //Act
            var result = service.Transfer(1,new TransferModel(){Amount = 100, ToUserId = 2});

            //Assert
            Assert.AreEqual(result.Status, RequestResultStatus.Error);
            Assert.AreEqual(result.Obj, 0);
            Assert.AreEqual(result.Message, "User is not found");
        }

        [Test]
        public void Transfer_NotEnoughMoney_Test()
        {
            //Arrange
            var userRepo = new Mock<IUserRepository>();
            userRepo.Setup(x => x.GetUser(1)).Returns(new User() { AccountBalance = 99, Transactions = new Collection<Transaction>() });
            userRepo.Setup(x => x.GetUser(2)).Returns(new User() { AccountBalance = 100, Transactions = new Collection<Transaction>() });

            var uow = new Mock<IUnitOfWork>();
            uow.Setup(x => x.UserRepository).Returns(userRepo.Object);

            var uowf = new Mock<IUnitOfWorkFactory>();
            uowf.Setup(x => x.CreateNewUnitOfWork()).Returns(uow.Object);

            var service = new BankAcountDataService(uowf.Object);

            //Act
            var result = service.Transfer(1,new TransferModel(){Amount = 100, ToUserId = 2});

            //Assert
            Assert.AreEqual(result.Status, RequestResultStatus.Error);
            Assert.AreEqual(result.Obj, 0);
            Assert.AreEqual(result.Message, "Not enough money");
        }

        [Test]
        public void Transfer_Success_Test()
        {
            //Arrange
            var userRepo = new Mock<IUserRepository>();
            userRepo.Setup(x => x.GetUser(1)).Returns(new User() { AccountBalance = 101, Transactions = new Collection<Transaction>() });
            userRepo.Setup(x => x.GetUser(2)).Returns(new User() { AccountBalance = 100, Transactions = new Collection<Transaction>() });

            var uow = new Mock<IUnitOfWork>();
            uow.Setup(x => x.UserRepository).Returns(userRepo.Object);

            var uowf = new Mock<IUnitOfWorkFactory>();
            uowf.Setup(x => x.CreateNewUnitOfWork()).Returns(uow.Object);

            var service = new BankAcountDataService(uowf.Object);

            //Act
            var result = service.Transfer(1,new TransferModel(){Amount = 100, ToUserId = 2});

            //Assert
            Assert.AreEqual(result.Status, RequestResultStatus.Success);
            Assert.AreEqual(result.Obj, 1);
        }
    }
}